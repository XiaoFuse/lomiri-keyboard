# Thai translation for lomiri-keyboard
# Copyright (c) 2015 Rosetta Contributors and Canonical Ltd 2015
# This file is distributed under the same license as the lomiri-keyboard package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: lomiri-keyboard\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-18 19:28+0000\n"
"PO-Revision-Date: 2021-05-23 05:10+0000\n"
"Last-Translator: Wannaphong Phatthiyaphaibun <wannaphong@yahoo.com>\n"
"Language-Team: Thai <https://translate.ubports.com/projects/ubports/keyboard-"
"component/th/>\n"
"Language: th\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 3.11.3\n"
"X-Launchpad-Export-Date: 2017-04-07 06:31+0000\n"

#: ../qml/ActionsToolbar.qml:56
msgid "Select All"
msgstr "เลือกทั้งหมด"

#: ../qml/ActionsToolbar.qml:57
msgid "Redo"
msgstr ""

#: ../qml/ActionsToolbar.qml:58
msgid "Undo"
msgstr "ถอยหลัง"

#: ../qml/ActionsToolbar.qml:73
msgid "Paste"
msgstr "วาง"

#: ../qml/ActionsToolbar.qml:74
msgid "Copy"
msgstr "คัดลอก"

#: ../qml/ActionsToolbar.qml:75
msgid "Cut"
msgstr "ตัด"

#: ../qml/FloatingActions.qml:62
msgid "Done"
msgstr "เสร็จสิ้น"

#: ../qml/Keyboard.qml:351
msgid "Swipe to move selection"
msgstr ""

#: ../qml/Keyboard.qml:352
msgid "Swipe to move cursor"
msgstr ""

#: ../qml/Keyboard.qml:352
msgid "Double-tap to enter selection mode"
msgstr ""

#: ../qml/keys/LanguageMenu.qml:106
msgid "Settings"
msgstr "ตั้งค่า"

#: ../qml/keys/languages.js:19
msgid "Arabic"
msgstr "อาหรับ"

#: ../qml/keys/languages.js:20
msgid "Azerbaijani"
msgstr "อาเซอร์ไบจาน"

#: ../qml/keys/languages.js:21
#, fuzzy
#| msgid "Hungarian"
msgid "Bulgarian"
msgstr "ฮังการี"

#: ../qml/keys/languages.js:22
msgid "Bosnian"
msgstr "บอสเนีย"

#: ../qml/keys/languages.js:23
msgid "Catalan"
msgstr "คาตาลัน"

#: ../qml/keys/languages.js:24
msgid "Czech"
msgstr "เช็ก"

#: ../qml/keys/languages.js:25
msgid "Danish"
msgstr "เดนมาร์ก"

#: ../qml/keys/languages.js:26
msgid "German"
msgstr "เยอรมัน"

#: ../qml/keys/languages.js:27
msgid "Emoji"
msgstr "อีโมจิ"

#: ../qml/keys/languages.js:28
msgid "Greek"
msgstr "กรีก"

#: ../qml/keys/languages.js:29
msgid "English"
msgstr "อังกฤษ"

#: ../qml/keys/languages.js:30
msgid "Esperanto"
msgstr ""

#: ../qml/keys/languages.js:31
msgid "Spanish"
msgstr "สเปน"

#: ../qml/keys/languages.js:32
msgid "Persian"
msgstr ""

#: ../qml/keys/languages.js:33
msgid "Finnish"
msgstr "ฟินแลนด์"

#: ../qml/keys/languages.js:34
msgid "French"
msgstr "ฝรั่งเศส"

#: ../qml/keys/languages.js:35
#, fuzzy
#| msgid "French"
msgid ""
"French\n"
"(Swiss)"
msgstr "ฝรั่งเศส"

#: ../qml/keys/languages.js:36
msgid "Scottish Gaelic"
msgstr "เกลิกสกอต"

#: ../qml/keys/languages.js:37
msgid "Hebrew"
msgstr "ฮีบรู"

#: ../qml/keys/languages.js:38
msgid "Croatian"
msgstr "โครเอเชีย"

#: ../qml/keys/languages.js:39
msgid "Hungarian"
msgstr "ฮังการี"

#: ../qml/keys/languages.js:40
msgid "Icelandic"
msgstr "ไอซ์แลนด์"

#: ../qml/keys/languages.js:41
msgid "Italian"
msgstr "อิตาลี"

#: ../qml/keys/languages.js:42
msgid "Japanese"
msgstr "ญี่ปุ่น"

#: ../qml/keys/languages.js:43
msgid "Lithuanian"
msgstr ""

#: ../qml/keys/languages.js:44
msgid "Latvian"
msgstr "ลัตเวีย"

#: ../qml/keys/languages.js:45
msgid "Korean"
msgstr ""

#: ../qml/keys/languages.js:46
msgid "Dutch"
msgstr "ดัตช์"

#: ../qml/keys/languages.js:47
msgid "Norwegian"
msgstr "นอร์เวย์"

#: ../qml/keys/languages.js:48
msgid "Polish"
msgstr "โปแลนด์"

#: ../qml/keys/languages.js:49
msgid "Portuguese"
msgstr "โปรตุเกส"

#: ../qml/keys/languages.js:50
msgid "Romanian"
msgstr "โรมาเนีย"

#: ../qml/keys/languages.js:51
msgid "Russian"
msgstr "รัสเซีย"

#: ../qml/keys/languages.js:52
msgid "Slovenian"
msgstr "สโลเวเนีย"

#: ../qml/keys/languages.js:53
msgid "Serbian"
msgstr "เซอร์เบีย"

#: ../qml/keys/languages.js:54
msgid "Swedish"
msgstr "สวีเดน"

#: ../qml/keys/languages.js:55
msgid "Turkish"
msgstr ""

#: ../qml/keys/languages.js:56
msgid "Ukrainian"
msgstr "ยูเครน"

#: ../qml/keys/languages.js:57
msgid ""
"Chinese\n"
"(Pinyin)"
msgstr ""
"จีน\n"
"(พินอิน)"

#: ../qml/keys/languages.js:58
msgid ""
"Chinese\n"
"(Chewing)"
msgstr ""
"จีน\n"
"(Chewing)"
